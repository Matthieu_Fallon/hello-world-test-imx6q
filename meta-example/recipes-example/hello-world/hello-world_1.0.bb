#
# This file was derived from the 'Hello World!' example recipe in the
# Yocto Project Development Manual.
#
 
DESCRIPTION = "Example Hello World application for Yocto build."
SECTION = "examples"
DEPENDS = ""
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1e029cd6af3106132d008e18626464ae;name=tarball"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

#commit for autotools and .service
SRCREV = "3886973f2da7f338e4988ce04936b46564a3a653"

SRC_URI = "git://Matthieu_Fallon@bitbucket.org/Matthieu_Fallon/hello-world-test-imx6q.git;protocol=https \
	   file://eth0.network \
	   file://foosball.AppImage \
	   file://hello-go \
           file://foosball.sh \
	   file://foosball.service \
	   file://socketcan.service \
	   file://tcp-client \
           "

SRC_URI[md5sum] = "097cbd11cda3db36a1a42f946c1b054d"

S = "${WORKDIR}/git"

#for go appliation
DEPENDS_append = " zlib"
RDEPENDS_${PN} = " fuse \
	bash \
	gtk+ \
	libxi \
	libxtst \
	libasound \
	libxscrnsaver \
	nss \
	nspr \
	gconf \
	cups \
	qtserialbus \
	qtbase"

#for Qt application tcp server -> qtbase, for Qt socketcan ->qtserialbus

inherit autotools systemd

INSANE_SKIP_${PN} = "ldflags"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"

do_install_append () {
	install -d ${D}${systemd_unitdir}/system/
	install -m 0755 ${S}/hello-world.service ${D}${systemd_unitdir}/system/
	install -m 0755 ${WORKDIR}/foosball.service ${D}${systemd_unitdir}/system/
	install -m 0755 ${WORKDIR}/socketcan.service ${D}${systemd_unitdir}/system/

	install -d ${D}${systemd_unitdir}/network/
	install -m 0755 ${WORKDIR}/eth0.network ${D}${systemd_unitdir}/network/
	
	install -d ${D}${bindir}/foosball
	install -m 0755 ${WORKDIR}/foosball.AppImage ${D}${bindir}/foosball/
	install -m 0755 ${WORKDIR}/hello-go ${D}${bindir}/foosball/
	#install -D -m 0755 ${WORKDIR}/linux-armv7l-unpacked/* ${D}${bindir}/foosball
	#cp -r ${WORKDIR}/linux-armv7l-unpacked/ ${D}${bindir}/foosball
	
	#install -d ${D}/etc/mini_x/session (finalement un service de ma conception lance ce script)
	install -m 0755 ${WORKDIR}/foosball.sh ${D}${bindir}

	install -d ${D}/usr/application/
	install -m 0755 ${WORKDIR}/tcp-client ${D}/usr/application/
}

FILES_${PN} += "${systemd_unitdir}/hello-world.service ${systemd_unitdir}/foosball.service ${systemd_unitdir}/socketcan.service ${systemd_unitdir}/network/eth0.network ${bindir}/foosball/foosball.AppImage ${bindir}/foosball/hello-go ${bindir}/foosball.sh /usr/application/tcp-client"

#Command for systemd ( visiblement il y a un probleme, elle n'est pas utile ou pas)
SYSTEMD_SERVICE_${PN} = "hello-world.service foosball.service socketcan.service"

#PARALLEL_MAKE = ""


